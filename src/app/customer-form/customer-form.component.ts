
import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';

import { Customer } from '../interfaces/customer';



import { CustomersService } from './../customers.service';
import { AuthService } from './../auth.service';
import { PredictService } from './../predict.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Capital } from '../interfaces/capital';
import { Country } from '../interfaces/country';
import { HousesService } from '../houses.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';




@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})

export class CustomerFormComponent implements OnInit {



onSubmit(){}
name;
phone;
region;
flag;
capital;
currencies;
userId;
languages;
timezones;
customers$;
population;
country;
customers;
action;
message="Details succesfully saved";
spinner:boolean = false;
seeAll:boolean = false;
seeCountry:boolean = false;
display;
countries$:Observable<Country>
capitales$:Observable<Capital>

getCountry(){
  console.log(this.country);
  this.seeCountry=true;
  this.countries$ = this.housesService.getCountries(this.country);
  this.countries$.subscribe(
    data => {
      this.country = data[0].name;
      this.flag=data[0].flag;
      this.timezones = data[0].timezones;
      this.capital = data[0].capital;
      this.population = data[0].population;
      console.log(data[0].languages[0].name);
      this.currencies = data[0].currencies[0].code;
      this.languages =  data[0].languages[0].name;
      
      
     

     } )
  
}

save(){

  this.customersService.addCustomer(this.userId,this.name,this.phone, this.country,this.population,this.languages,this.currencies);
  this.router.navigate(['/customers']);

}

openSnackBar() {
  this._snackBar.open(this.message,this.action,{
    duration: 2000,
  });
}

constructor(private http: HttpClient,private _snackBar:MatSnackBar, private router:Router, private housesService:HousesService, private authService:AuthService, private customersService:CustomersService) { }






ngOnInit(): void {
  this.authService.getUser().subscribe(
    user => {
        this.userId = user.uid;
        this.customers$ = this.customersService.getCustomers(this.userId);
        this.customers$.subscribe(
          docs => {         
            this.customers = [];
            var i = 0;
            for (let document of docs) {
              console.log(i++); 
              const customer:Customer = document.payload.doc.data();
              customer.id = document.payload.doc.id;
                 this.customers.push(customer); 
                 console.log(customer);
            }                        
          }
        )
    })
}   
}