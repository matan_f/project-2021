export interface Capital {
    name:string,
    capital:string,
    flag:string,
    population:string,
    timezones:string,
    currencies:string,
    languages:string
}
