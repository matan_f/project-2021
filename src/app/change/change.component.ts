import { ChangeService } from './../change.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-change',
  templateUrl: './change.component.html',
  styleUrls: ['./change.component.css']
})
export class ChangeComponent implements OnInit {

  currencyList=["USD","JPY","ARS","BGN","CZK","DKK","GBP","HUF","PLN","RON" ,"SEK" ,"CHF","ISK" ,"NOK" ,"HRK","RUB","TRY"	,"AUD","BRL","CAD","CNY" ,"HKD","IDR","ILS","INR","KRW","MXN","MYR","NZD","PHP","SGD","THB","ZAR"] 
  currency1;
  currency2;
  price;
  changes$;
  ans;
  convert:boolean=false;
  
  change(){
    this.convert= true;
    console.log(this.currency1);
    console.log(this.currency2);
    console.log(this.price);
    this.changes$ = this.changeService.getChange(this.currency1,this.currency2);
    let cuu = this.currency1+"_"+this.currency2;
    this.changes$.subscribe(
      data => {
        this.ans = (data[cuu]*this.price).toFixed(2);
        console.log(this.ans);
      })}

  
  constructor(private changeService:ChangeService) { }

  ngOnInit(): void {
  }

}
