import { HttpClient } from '@angular/common/http';
import {  Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class ChangeService {
  
  private URL = "https://free.currconv.com/api/v7/convert?q="; 
  private KEY = "0da4cbaa5e0ef9aaa08e";
  

  getChange(currency1:string,currency2:string):Observable<any>{
    return this.http.get<any>(`${this.URL}${currency1}_${currency2}&compact=ultra&apiKey=${this.KEY}`)
  }



  
  constructor(private http:HttpClient, public router:Router) { }
  
}