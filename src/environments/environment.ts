// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB29XDc9JJvWZlfs6_uGCys4UEepyils4I",
    authDomain: "project-c6c01.firebaseapp.com",
    projectId: "project-c6c01",
    storageBucket: "project-c6c01.appspot.com",
    messagingSenderId: "5634379545",
    appId: "1:5634379545:web:4334298f1719856dc57f88"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
